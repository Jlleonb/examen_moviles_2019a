package com.example.game_calculator

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_operation.*
import org.w3c.dom.Text
import kotlin.random.Random

class operation : Fragment() {

    internal lateinit var titleScreen: TextView
    internal lateinit var roundText:TextView
    internal lateinit var scoreText:TextView
    internal lateinit var timeLeftText:TextView
    internal lateinit var signText:TextView
    internal lateinit var firstNumber:TextView
    internal lateinit var secondNumber:TextView
    internal lateinit var response:TextView

    private var listener: OnFragmentInteractionListener? = null

    private val args: operationArgs by navArgs()

    private var initialCountdown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    var round = 1
    var score = 0
    var timeLeft = 10
    var upNumber:Int = 0
    var downNumber:Int = 0
    var submitted = ""
    var resolved = 0.0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val operation = args.operation
        titleScreen = view.findViewById(R.id.titleText)
        titleScreen.text = operation.toString()
        titleScreen.text = getString(R.string.titleBar, operation)

        roundText = view.findViewById(R.id.roundText)
        roundText.text = getString(R.string.round, round.toString())

        scoreText = view.findViewById(R.id.scoreText)
        scoreText.text = getString(R.string.score, score.toString())

        timeLeftText = view.findViewById(R.id.timeLeftText)
        timeLeftText.text = getString(R.string.timeLeft, timeLeft.toString())

        response = view.findViewById(R.id.responseText)

        goButton.setOnClickListener{
            startGameButtonPressed()
            newNumbers(view)
        }

        submitButton.setOnClickListener{
            submitted = response.text.toString()
            resolved = submitted.toDouble()
            var inSco = correctValue(resolved)
            if (inSco){
                incrementScore()
            }
            changeRound()
            newNumbers(view)
            if (round == 6){
                finishGame(view)
            }
        }

        signChange(view)
    }

    fun newNumbers (view:View){
        upNumber = Random.nextInt(1,5)
        downNumber = Random.nextInt(1, 5)
        firstNumber = view.findViewById(R.id.firstNumber)
        firstNumber.text = getString(R.string.firstNumber, upNumber.toString())
        secondNumber = view.findViewById(R.id.secondNumber)
        secondNumber.text = getString(R.string.secondNumber, downNumber.toString())

    }

    fun finishGame (view:View){
        Toast.makeText(activity, getString(R.string.end_game, score.toString()), Toast.LENGTH_LONG).show()
        round = 1
        roundText = view.findViewById(R.id.roundText)
        roundText.text = getString(R.string.round, round.toString())

        score = 0
        scoreText = view.findViewById(R.id.scoreText)
        scoreText.text = getString(R.string.score, score.toString())

        timeLeft = 10
        timeLeftText = view.findViewById(R.id.timeLeftText)
        timeLeftText.text = getString(R.string.timeLeft, timeLeft.toString())

        countDownTimer.cancel()

        response = view.findViewById(R.id.responseText)
        response.text = ""

        submitButton.isEnabled = false
        goButton.isEnabled = true

    }

    fun correctValue (submit:Double):Boolean{
        var correct:Boolean = false
        if (args.operation == "Adittions")
            correct = (submit == (upNumber + downNumber).toDouble())
        else if (args.operation == "Substractions")
            correct = (submit == (upNumber - downNumber).toDouble())
        else if (args.operation == "Multiplications")
            correct = (submit == (upNumber * downNumber).toDouble())
        else if (args.operation == "Divisions")
            correct = (submit == (upNumber / downNumber).toDouble())
        return correct
    }

    fun signChange (view:View){
        signText = view.findViewById(R.id.signText)
        if (args.operation == "Adittions")
            signText.text = getString(R.string.sign, "+")
        else if (args.operation == "Substractions")
            signText.text = getString(R.string.sign, "-")
        else if (args.operation == "Multiplications")
            signText.text = getString(R.string.sign, "x")
        else if (args.operation == "Divisions")
            signText.text = getString(R.string.sign, "÷")
    }

    fun startGameButtonPressed (){
        submitButton.isEnabled = true
        goButton.isEnabled = false

        restartCount()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operation, container, false)
    }

    fun incrementScore(){

        if (timeLeft >= 8 && timeLeft < 10)
            score+=100
        else if (score >= 5 && timeLeft <8)
            score+=50
        else if (score >= 0 && timeLeft<5)
            score +=10
        scoreText.text = getString(R.string.score, score.toString())
        response.text = ""
        countDownTimer.cancel()
    }


    fun restartCount(){
        timeLeft = 10
        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftText.text = getString(R.string.timeLeft, timeLeft.toString())
            }

            override fun onFinish() {
                timeLeft=10
                changeRound()
            }

        }
        countDownTimer.start()
    }

    fun changeRound(){
        round++
        roundText.text = getString(R.string.round, round.toString())
        restartCount()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

}
