package com.example.game_calculator

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController

class Menu : Fragment() {
    // TODO: Rename and change types of parameters

    private var listener: OnFragmentInteractionListener? = null
    internal lateinit var adittionButton: Button
    internal lateinit var substractionButton: Button
    internal lateinit var multiplicationButton: Button
    internal lateinit var divisionButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adittionButton = view.findViewById(R.id.adittionButton)
        adittionButton.setOnClickListener{goToOperationScreen("Adittions")}
        substractionButton = view.findViewById(R.id.substractionButton)
        substractionButton.setOnClickListener{goToOperationScreen("Substractions")}
        multiplicationButton = view.findViewById(R.id.multiplicationButton)
        multiplicationButton.setOnClickListener{goToOperationScreen("Multiplications")}
        divisionButton = view.findViewById(R.id.divisionButton)
        divisionButton.setOnClickListener{goToOperationScreen("Divisions")}
    }

    fun goToOperationScreen (operation:String){
        val action = MenuDirections.actionMenuToOperation(operation)
        view?.findNavController()?.navigate(action)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

}
